package proyecto3erciclocitas;
public class Personal extends Clinica{
    private String NOMBRE;
    private String TELEFONO;
    private String CEDULA;
    public Personal(String NOMBRE, String TELEFONO, String CEDULA){
      this.NOMBRE=NOMBRE;
      this.TELEFONO=TELEFONO;
      this.CEDULA=CEDULA;
    }
    public Personal() {
    } 
    public String getNOMBRE() {
        return NOMBRE;
    }
    public void setNOMBRE(String NOMBRE) {
        this.NOMBRE = NOMBRE;
    }
    public String getTELEFONO() {
        return TELEFONO;
    }
    public void setTELEFONO(String TELEFONO) {
        this.TELEFONO = TELEFONO;
    }
    public String getCEDULA() {
        return CEDULA;
    }
    public void setCEDULA(String CEDULA) {
        this.CEDULA = CEDULA;
    }
    @Override
    public double sueldo(){
        
        return 0;
    }  
}
