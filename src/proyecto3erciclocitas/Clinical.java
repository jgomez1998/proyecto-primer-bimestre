package proyecto3erciclocitas;
import java.util.*;//para el escaner lectura y entrada en consola
import java.io.*;//Para trabajar los archivos, define distintos flujos de datos
public class Clinical {
    public static String DESCRIPCION;
    static File ArchivoTXT = new File("Database.txt");
    public static void main(String[] args) {
        comprobarArchivo();//Metodo para comprobar si el Archivo txt ya esta creado o si ya se creo uno
        menuOpcionesPrincipal();  
    }
    //Comprobar si el archivo existe
    public static void comprobarArchivo() {
        try {//Para el manejo de archivos
            if (ArchivoTXT.exists()) {//Si el archivo existe hay como llenar el archivo
                System.out.println("» » » » » » » » PUEDE TRABAJAR EN EL ARCHIVO « « « « « « « «");
            } else {//Sino se creara un Archivo TXT
                ArchivoTXT.createNewFile();
                System.out.println("ARCHIVO CREADO, YA PUEDES TRABAJAR");
            }
            System.out.println("");
        } catch (IOException e) {// Si hay algun problema en el manejo de archivos
            System.out.println("Error" + e.getMessage());
            System.out.println("NO SE PUEDE TRABAJAR EN EL ARCHIVO\n");
        }
    }
    //Menu de opciones 
    public static void menuOpcionesPrincipal() {
        Scanner teclado = new Scanner(System.in);
        int opcion;
        boolean aux = true;
        do {
            //Se escoge una opcion para hacer lo que se desea en una de estas opciones
            System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
            System.out.println("---------------------AGENDA DE CITAS---------------------");
            System.out.println("-----HORARIO ATENCIÓN: 08:00 - 13:00 Y 15:00 - 19:00-----");
            System.out.println("---------------------FORMATO 24 HORAS--------------------");
            System.out.println("» » » ESCOGA UNA DE LAS SIGUIENTES OPCIONES « « «");
            System.out.println("1. MOSTRAR TODAS LAS CITAS Y DISPONIBLIDAD");
            System.out.println("2. AGREGAR CITA");
            System.out.println("3. BUSCAR CITA");
            System.out.println("4. ELIMINAR CITAS");
            System.out.println("5. SALIR");
            //Lee la opciones que se escoge
            System.out.print("» » "); opcion = teclado.nextInt();
            if (opcion == 5) {
                break;
            }
            //Realiza acciones diferentes según la opción que se escogio y cada una de ellas invoca a sus diferentes mÉtodos 
            switch (opcion) {
                case 1:
                    //Mostar todos los registros agregados
                    System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
                    System.out.println("» » » » » » » » Total Citas « « « « « « « «");
                    mostrarRegistros();
                    System.out.println("» » » » » » » » Fin Total Citas « « « « « « « «");
                    break;
                case 2:
                    //Agregar registro de cita nueva
                    System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
                    System.out.println("» » » » » » » » AGENDAR CITA « « « « « « « «");
                    System.out.println("» » » » » » » FORMATO 24 HORAS « « « « « « «");
                    agregarRegistro();
                    break;
                case 3:
                    //Buscar una cita ya agregada
                    System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
                    System.out.println("» » » » » » » » Buscando.... « « « « « « « «");
                    menuBuscarRegistro();
                    break;
                case 4:
                    //Eliminar cita 
                    System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
                    System.out.println("» » » » » » » » Eliminar Cita « « « « « « « «");
                    eliminarRegistro();
                    break;     
                case 5:
                    System.out.println("SALIR");
                    aux = false;
                    break;
                    //En esta opcíon se sale del programa
                default:
                    //Si no es ninguna opcíon de las ya predefinidas nos preentara un mensaje de error.
                    System.out.println("OPCIÓN NO VÁLIDA VUELVA A INGRESAR");
                    menuOpcionesPrincipal();
                    break;
            }
        } while (opcion == 5);
    }
    //Mostrar todos los registros
    public static void mostrarRegistros() {
        //Variables datos = new Variables ();
        try {//No tiene parametros porque no necesita informacion previa par apoder continuar y mostrar los registros
            String linea;
            //buffereReader nos permite leer linea completa , cuando termina de leer devuelve null
            BufferedReader br = new BufferedReader(new FileReader(ArchivoTXT));
            while ((linea = br.readLine()) != null) {
                //StringTokenizer ayuda a dividir strigns en base a otro string"caracter" o delimitador 
                StringTokenizer mistokens = new StringTokenizer(linea, "|");
                while (mistokens.hasMoreTokens()) {//Si depsues de almacenar datos siguen habiendo mas los sigue almacenando
                    String tamRegistro = mistokens.nextToken();
                    //Va leyendo todos los datos
                    String NOMBRE = mistokens.nextToken();
                    String TELEFONO = mistokens.nextToken();
                    String CEDULA = mistokens.nextToken();
                    String DIRECCION = mistokens.nextToken();
                    String FECHA = mistokens.nextToken();
                    String HORA = mistokens.nextToken();
                    DESCRIPCION = mistokens.nextToken();
                    //Imprime los datos encontrados en el Archivo de texto de las citas agendadas
                    System.out.println("El nombre es:\t\t" + NOMBRE);
                    System.out.println("El teléfono es:\t\t" + TELEFONO);
                    System.out.println("La cédula es:\t\t" + CEDULA);
                    System.out.println("La dirección es:\t" + DIRECCION);
                    System.out.println("La fecha es:\t\t" + FECHA);
                    System.out.println("La hora es:\t\t" + HORA);
                    System.out.println("Motivo de visita es:\t" + DESCRIPCION);
                    System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        menuOpcionesPrincipal();
    }
    //Agregar registro
    public static void agregarRegistro() {
        Usuario datos = new Usuario ();
        Scanner teclado = new Scanner(System.in);
        String sepCampo = "|";//Separador de campo
        int tamRegistro = 0; // Aqui calculamos el tamaño del registro
        //int motivo;
        try {
            //Bufferedwiter para escribir sobre ficheros 
            //True por si tenemos datos en el archivo que siga escribiendo
            //Fescribe funcion para escribir en el archivo, es una instancia(objeto) de la clase
            //Instancia una clase, es crear un objeto dentro de otra clase
            //FileOutputStream sirve para escribir en el archivo datos orientados a caracteres
            BufferedWriter Fescribe = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(ArchivoTXT,true)));
            //Nos permite dentro del archivo
            //Aqui en estas variables se lee los datos necesarios para agendar la cita
            //OutputStreamWriter una clase que permite la conversione entre stream de bytes a caracteres
            //Stream es para entrada y salida de datos que permite la comuncacion de informacion entre la fuente y su destino
            //Permite la apertura del archivo para poder escribir en el, almacenar un stream de datos sobre un fichero
            System.out.print("INGRESE SU NOMBRE COMPLETO:\t\t "); datos.setNOMBRE(teclado.nextLine());
            System.out.print("INGRESE SU TELEFONO:\t\t\t "); datos.setTELEFONO(teclado.nextLine());
            System.out.print("INGRESE SU Nº DE CÉDULA:\t\t "); datos.setCEDULA(teclado.nextLine());
            System.out.print("INGRESE DIRECCIÓN:\t\t\t "); datos.setDIRECCION(teclado.nextLine());
            System.out.print("INGRESE FECHA DE LA CITA (DD/MM/AA):\t "); datos.setFECHA(teclado.nextLine());
            System.out.print("INGRESE HORA DE LA CITA (HH/MM):\t "); datos.setHORA(teclado.nextLine());
            //Muestra diferentes motivos por los que se puede ir al centro dermatológico
            System.out.println("INGRESE MOTIVO DE LA CITA:\t\t1. Depilación definitiva de: Bigote, Axilas, Cara o Bikini");
            System.out.println("\t\t\t\t\t2. Acné activo");
            System.out.println("\t\t\t\t\t3. Nevus de ota");
            System.out.println("\t\t\t\t\t4. Alopecia o Caída de cabello");
            System.out.println("\t\t\t\t\t5. Acido hialurónico");
            System.out.println("\t\t\t\t\t6. Radio Frecuencia");
            System.out.println("\t\t\t\t\t7. Venitas Superficiales");
            System.out.println("\t\t\t\t\t8. Mesolifting");
            System.out.println("\t\t\t\t\t9. Otro motivo");
            //Esta variable lee el valor ingresado por el motivo de la visita
            System.out.print("» » » » » » » » "); datos.setMotivo(teclado.nextInt());
            switch (datos.getMotivo()) {
                //Cualquiera de los motivos escogidos se guarda en la variable String DESCRIPCION e imprimi el costo
                case 1:
                    DESCRIPCION = "Depilación definitiva de: Bigote, Axilas, Cara o Bikini";
                    System.out.println("REGISTRADO CON ÉXITO");
                    System.out.println("EL PRECIO A CANCELAR ES DE $50 DÓLARES ");
                    break;
                case 2:
                    DESCRIPCION = "Acné Activo";
                    System.out.println("REGISTRADO CON ÉXITO");
                    System.out.println("EL PRECIO A CANCELAR ES DE $50 DÓLARES ");
                    break;
                case 3:
                    DESCRIPCION = "Nevus de ota";
                    System.out.println("REGISTRADO CON ÉXITO");
                    System.out.println("EL PRECIO A CANCELAR ES DE $60 DÓLARES ");
                    break;
                case 4:
                    DESCRIPCION = "Alopecia o Caída de cabello";
                    System.out.println("REGISTRADO CON ÉXITO");
                    System.out.println("EL PRECIO A CANCELAR ES DE $80 DÓLARES ");
                    break;
                case 5:
                    DESCRIPCION = "Acido hialurónico";
                    System.out.println("REGISTRADO CON ÉXITO");
                    System.out.println("EL PRECIO A CANCELAR ES DE $30 DÓLARES ");
                    break;
                case 6:
                    DESCRIPCION = "Radio Frecuencia";
                    System.out.println("REGISTRADO CON ÉXITO");
                    System.out.println("EL PRECIO A CANCELAR ES DE $60 DÓLARES ");
                    break;
                case 7:
                    DESCRIPCION = "Venitas Superficiales";
                    System.out.println("REGISTRADO CON ÉXITO");
                    System.out.println("EL PRECIO A CANCELAR ES DE $25 DÓLARES ");
                    break;
                case 8:
                    DESCRIPCION = "Mesolifting";
                    System.out.println("REGISTRADO CON ÉXITO");
                    System.out.println("EL PRECIO A CANCELAR ES DE $25 DÓLARES ");
                    break;
                case 9:
                    //Si el motivo de la cita aun no s elo tiene claro se puede ir y el precio lo definira el Dermatolog@
                    DESCRIPCION = "Otro motivo";
                    System.out.println("REGISTRADO CON ÉXITO");
                    System.out.println("PRECIO POR DEFINIRSE ");
                    break;
            }   
            //Se mide el tamaño del registro
            //length para obterner la longitud del campo y almacenar en la variable tamRegistro
            tamRegistro = sepCampo.length() + datos.getNOMBRE().length() + sepCampo.length() + datos.getTELEFONO().length() 
                    + sepCampo.length() + datos.getCEDULA().length() + sepCampo.length() + datos.getDIRECCION().length() 
                    + sepCampo.length() + datos.getFECHA().length() + sepCampo.length() + datos.getHORA().length() 
                    + sepCampo.length() + DESCRIPCION.length() + sepCampo.length();
            //Metodo para escribir en el archivo
            Fescribe.write(sepCampo + tamRegistro + sepCampo + datos.getNOMBRE() + sepCampo + datos.getTELEFONO() + sepCampo
                    + datos.getCEDULA() + sepCampo + datos.getDIRECCION() + sepCampo + datos.getFECHA() + sepCampo + 
                    datos.getHORA() + sepCampo + DESCRIPCION + sepCampo);
            //Metodo para cerrar el archivo
            Fescribe.close();//Cerramos el fichero
            menuOpcionesPrincipal();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    //MENU CON OPCIONES PARA BUSCAR 
    public static void menuBuscarRegistro() {
        Scanner teclado = new Scanner(System.in);
        int opc;
        do {
            //Me muestra las opciones de búsqueda disponibles o para regresar al metodo de las opciones principales
            System.out.println("INGRESE CRITERIO DE BÚSQUEDA");
            System.out.println("1. BÚSQUEDA POR CÉDULA");
            System.out.println("2. REGRESAR AL MENÚ PRINCIPAL");
            System.out.print("» » "); opc = teclado.nextInt();
            switch (opc) {
                case 1:
                    buscarRegistroCedula();//Me manda al metodo buscarRegistroCedula;
                    break;
                case 2:
                    menuOpcionesPrincipal();//Me manda al menuOpcionesPrincipal
                    break;
                    //Si la opcion no es vlaida nos mostrara un mensaje de OPCION NO VALIDA
                default:
                    System.out.println("!!!!!!!!!!! OPCIÓN NO VÁLIDA !!!!!!!!!!!");
                    break;
            }
        } while (opc != 2);
    }
    //Buscar Registro por numero de cedula
    public static void buscarRegistroCedula() {
        Scanner teclado = new Scanner(System.in);
        String buscar, leer;
        boolean encontrado = true;
        try {//Se ingresa las sentencias para validar si tiene un error de ejecucion
            //buffereReader nos permite leer linea completa , cuando termina de leer devuelve null
            BufferedReader br = new BufferedReader(new FileReader(ArchivoTXT));//Para almacenar la lectura del archivo
            //File reader es para leer ficheros de texto
            System.out.print("INGRESE Nº DE CÉDULA: "); buscar = teclado.nextLine();
            while ((leer = br.readLine()) != null) {//Para que lea el archivo hasta que este no contenga nada y almacenaremos
                // lo que tenga el archivo en la variable leer 
                StringTokenizer st = new StringTokenizer(leer, "|");//Para separar los campos del registro 
                if (leer.contains(buscar)) {//Nos ayuda a comprobar lo que estamos buscando en la base de datos  
                    String cedula = null;
                    while (cedula != buscar && encontrado) {//Mientras cedula es diferente de buscar seguira buscando 
                        String tamregistro = st.nextToken();
                        String nombre = st.nextToken();
                        String telefono = st.nextToken();
                        cedula = st.nextToken();
                        String direccion = st.nextToken();
                        String fecha = st.nextToken();
                        String hora = st.nextToken();
                        String descripcion = st.nextToken();
                        if (cedula.equals(buscar)) {
                            encontrado = false;//Para ya no seguir almacenando mas datos en los tokens 
                            System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
                            System.out.println("» » » » » » RESULTADO DE LA BÚSQUEDA « « « « « «");
                            System.out.println("El nombre es:\t\t " + nombre);
                            System.out.println("El telefono es:\t\t " + telefono);
                            System.out.println("La cédula es:\t\t " + cedula);
                            System.out.println("La dirección es:\t " + direccion);
                            System.out.println("La fecha de la cita es:\t " + fecha);
                            System.out.println("La hora de la cita es:\t " + hora);
                            System.out.println("Motivo de visita es:\t " + descripcion);
                            System.out.println("» » » » » » » » Busqueda Finalizada « « « « « « « «");
                            System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
                        }
                    }
                } else {
                    System.out.println("El resultado de la busqueda " + buscar + " no existe");
              //Si no encuentra lo que estamos buscando nos imprimi ese mensaje y nos vuelve a mostrar el menuBuscarRegistro()
                }
                br.close();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());//muestra el mensaje del tipo de error que tiene sin parar el programa
        }
    }
    
    //Método para Eliminar una cita Registrada
    public static void eliminarRegistro() {
        Scanner sc = new Scanner(System.in);
        String linea, buscar;
        String sepreg = "|";
        Vector lineasAcopiar = new Vector();//Clase Vector para almacenar lineas a copiar
        //La variable buscar va a recoger el numero de cedula para buscar datos relacionados con ese numero de cedula
        System.out.print("INGRESE NUMERO DE CEDULA DE PACIENTE QUE DESEA ELIMINAR LA CITA: "); buscar = sc.nextLine();
        try {
            //FileReader fr = new FileReader(ArchivoTXT);//Para leer el archivo
            BufferedReader br = new BufferedReader(new FileReader(ArchivoTXT));
            while (br.ready()) {//Si el buffer no esta vacio devuelve true
                linea = br.readLine();//Almacena todo lo que lee en el archivo en esta variable 
                StringTokenizer mistokens = new StringTokenizer(linea, "|");
                if (linea.contains(buscar)) {//devulce un vlaor booleano
                    while (mistokens.hasMoreTokens()) {//comprueba si hay mas tokens por recorrer
                        String tamRegistro = mistokens.nextToken();
                        String nombre = mistokens.nextToken();
                        String telefono = mistokens.nextToken();
                        String cedula = mistokens.nextToken();
                        String direccion = mistokens.nextToken();
                        String fecha = mistokens.nextToken();
                        String hora = mistokens.nextToken();
                        String descripcion = mistokens.nextToken();
                        //Aqui vamos almacenando los registro que leyo antes
                        String registros =sepreg + tamRegistro + sepreg + nombre + sepreg + telefono + sepreg + cedula 
                        +sepreg + direccion + sepreg + fecha + sepreg + hora + sepreg + descripcion;
                        if (!cedula.equals(buscar)) {
                        //Si la cedula es diferente o = a lo que estamos buscando lo vamos descartando del vector 
                            lineasAcopiar.add(registros);
                        } else {
                            //Imprimira los datos del paciente del cual se elimine su registro
                            System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
                            System.out.println("» » » » » » EL REGISTRO ELIMINADO ES EL SIGUIENTE");
                            System.out.println("El nombre es:\t\t " + nombre);
                            System.out.println("El telefono es:\t\t " + telefono);
                            System.out.println("La cédula es:\t\t " + cedula);
                            System.out.println("La dirección es:\t " + direccion);
                            System.out.println("La fecha de la cita es:\t " + fecha);
                            System.out.println("La hora de la cita es:\t " + hora);
                            System.out.println("Motivo de visita es:\t " + descripcion);
                            System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
                        }
                    }
                } else {
                    System.out.println("EL PACIENTE CON EL NUMERO DE CEDULA " + buscar + " NO EXISTE");
                }
                br.close();
                //SE RECORRE EL VECTOR Y SE GUARDAN LAS LINEAS EN EL FICHERO
                if (linea.contains(buscar)) {
                    BufferedWriter bw = new BufferedWriter(new FileWriter(ArchivoTXT));
                    for (int i = 0; i < lineasAcopiar.size(); i++) {//Recorrer el vextor
                        linea = (String) lineasAcopiar.elementAt(i);//Se guarda en linea cada string del vector
                        bw.write(linea);//Se escribe la linea en el fichero
                    }
                    bw.close();
                    System.out.println("EL REGISTRO CON NUMERO DE CEDULA " + buscar + " HA SIDO ELIMINADO");
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        menuOpcionesPrincipal();
    }
}
