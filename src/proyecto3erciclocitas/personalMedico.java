package proyecto3erciclocitas;

import java.util.*;//para el escaner lectura y entrada en consola
import java.io.*;//Para trabajar los archivos, define distintos flujos de datos

class personalMedico extends Personal {
    static File ArchivoTXT = new File("DOCTORES.txt");
    private String ESPECIALIDAD;
    private String GRADOACADEMICO;
    private int Motivo;
    
    public personalMedico() {
    }
    
    public personalMedico(String NOMBRE, String TELEFONO, String CEDULA, String ESPECIALIDAD, String GRADOACADEMICO) {
        super(NOMBRE, TELEFONO, CEDULA);
        this.ESPECIALIDAD = ESPECIALIDAD;
        this.GRADOACADEMICO = GRADOACADEMICO;
    }

    public String getESPECIALIDAD() {
        return ESPECIALIDAD;
    }

    public void setESPECIALIDAD(String ESPECIALIDAD) {
        this.ESPECIALIDAD = ESPECIALIDAD;
    }

    public String getGRADOACADEMICO() {
        return GRADOACADEMICO;
    }

    public void setGRADOACADEMICO(String GRADOACADEMICO) {
        this.GRADOACADEMICO = GRADOACADEMICO;
    }

    /*public static void PersonalNuevo() {
        comprobarArchivo();
        menuOpcionesPrincipal();
    }
    }*/
    @Override
    public double sueldo() {
        return super.sueldo(); //To change body of generated methods, choose Tools | Templates.
    }

}
